DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS board;
DROP TABLE IF EXISTS play_area_template;
DROP TABLE IF EXISTS play_area;
DROP TABLE IF EXISTS cell_template;
DROP TABLE IF EXISTS cell;
DROP TABLE IF EXISTS terrain_type;
DROP TABLE IF EXISTS object_class;
DROP TABLE IF EXISTS unit_class;
DROP TABLE IF EXISTS factory_class;
DROP TABLE IF EXISTS factory_class_cost;
DROP TABLE IF EXISTS resource_class;
DROP TABLE IF EXISTS component_class;
DROP TABLE IF EXISTS body_class;
DROP TABLE IF EXISTS container_class;
DROP TABLE IF EXISTS melee_class;
DROP TABLE IF EXISTS armor_class;
DROP TABLE IF EXISTS cannon_class;
DROP TABLE IF EXISTS projectile_class;
DROP TABLE IF EXISTS effect_class;
DROP TABLE IF EXISTS resource_type;
DROP TABLE IF EXISTS damage_type;
DROP TABLE IF EXISTS armor_type;
DROP TABLE IF EXISTS element;
DROP TABLE IF EXISTS material;
DROP TABLE IF EXISTS element_relation;
DROP TABLE IF EXISTS damage_type_armor_type;
DROP TABLE IF EXISTS damage_type_material;
DROP TABLE IF EXISTS element_material;
DROP TABLE IF EXISTS damage_effect;
DROP TABLE IF EXISTS container_allowed_object;
DROP TABLE IF EXISTS cannon_allowed_projectiles;
DROP TABLE IF EXISTS object;
DROP TABLE IF EXISTS unit;
DROP TABLE IF EXISTS factory;
DROP TABLE IF EXISTS factory_cost;
DROP TABLE IF EXISTS resource;
DROP TABLE IF EXISTS component;
DROP TABLE IF EXISTS body;
DROP TABLE IF EXISTS container;
DROP TABLE IF EXISTS melee;
DROP TABLE IF EXISTS armor;
DROP TABLE IF EXISTS cannon;
DROP TABLE IF EXISTS projectile;
DROP TABLE IF EXISTS turn;
DROP TABLE IF EXISTS move;
DROP TABLE IF EXISTS action_type;
DROP TABLE IF EXISTS cell_state;,


CREATE TABLE user (,
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE game (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  board_id INTEGER,
  player1_id INTEGER NOT NULL,
  player2_id INTEGER NOT NULL,
  name TEXT NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (player1_id) REFERENCES user (id),
  FOREIGN KEY (player2_id) REFERENCES user (id),
  FOREIGN KEY (board_id) REFERENCES board_template (id)
);

CREATE TABLE board (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL
);

CREATE TABLE play_area_template (
  board_id INTEGER NOT NULL,
  id INTEGER NOT NULL,
  name TEXT NOT NULL,
  PRIMARY KEY (board_id,id),
  FOREIGN KEY (board_id) REFERENCES board (id)
);

CREATE TABLE play_area (
  game_id INTEGER NOT NULL,
  id INTEGER NOT NULL,
  name TEXT NOT NULL,
  PRIMARY KEY (game_id,id),
  FOREIGN KEY (game_id) REFERENCES game (id)
);

CREATE TABLE cell_template (
  board_id INTEGER NOT NULL,
  play_area_id INTEGER NOT NULL,
  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  terrain_id INTEGER NOT NULL,
  PRIMARY KEY (board_id,play_area_id,x,y),
  FOREIGN KEY (board_id) REFERENCES board (id),
  FOREIGN KEY (board_id,play_area_id) REFERENCES play_area_template (board_id,id),
  FOREIGN KEY (terrain_id) REFERENCES terrain (id)
);

CREATE TABLE cell (
  game_id INTEGER NOT NULL,
  play_area_id INTEGER NOT NULL,
  turn_id INTEGER NOT NULL,
  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  terrain_type_id INTEGER NOT NULL,
  height INTEGER NOT NULL,
  PRIMARY KEY (game_id,play_area_id,x,y),
  FOREIGN KEY (game_id) REFERENCES game (id),
  FOREIGN KEY (turn_id) REFERENCES turn (id),
  FOREIGN KEY (game_id,play_area_id) REFERENCES play_area (game_id,id),
  FOREIGN KEY (terrain_type_id) REFERENCES terrain_type (id)
);

CREATE TABLE terrain_type (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  speed INTEGER NOT NULL,
  name TEXT NOT NULL
);

CREATE TABLE object_class (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name TEXT NOT NULL,
  material_id INTEGER NOT NULL,
  unit_class_id INTEGER,
  factory_class_id INTEGER,
  resource_class_id INTEGER,
  component_class_id INTEGER,
  body_class_id INTEGER,
  container_class_id INTEGER,
  cannon_class_id INTEGER,
  melee_class_id INTEGER,
  projectile_class_id INTEGER,
  armor_class_id INTEGER,
  effect_class_id INTEGER,
  interactible BOOLEAN NOT NULL,
  has_children BOOLEAN NOT NULL,
  FOREIGN KEY (unit_class_id) REFERENCES unit_class (id),
  FOREIGN KEY (factory_class_id) REFERENCES factory_class (id),
  FOREIGN KEY (resource_class_id) REFERENCES resource_class (id),
  FOREIGN KEY (component_class_id) REFERENCES component_class (id),
  FOREIGN KEY (body_class_id) REFERENCES body_class (id),
  FOREIGN KEY (container_class_id) REFERENCES container_class (id),
  FOREIGN KEY (cannon_class_id) REFERENCES cannon_class (id),
  FOREIGN KEY (melee_class_id) REFERENCES melee_class (id),
  FOREIGN KEY (projectile_class_id) REFERENCES projectile_class (id),
  FOREIGN KEY (armor_class_id) REFERENCES armor_class (id),
  FOREIGN KEY (effect_class_id) REFERENCES effect_class (id)
);

CREATE TABLE unit_class (
  id INTEGER PRIMARY KEY ,
  name TEXT NOT NULL,
  speed INTEGER NOT NULL,
  range INTEGER NOT NULL,
  defense INTEGER NOT NULL,
  attack INTEGER NOT NULL
);

CREATE TABLE factory_class (
  id INTEGER PRIMARY KEY,
  cool_down INTEGER NOT NULL,
  batch_size INTEGER NOT NULL
);

CREATE TABLE factory_class_cost (
  factory_class_id INTEGER PRIMARY KEY,
  resource_type_id INTEGER NOT NULL,
  cost INTEGER NOT NULL,
  FOREIGN KEY (factory_class_id) REFERENCES factory_class (id),
  FOREIGN KEY (resource_type_id) REFERENCES resource_type (id)
);

CREATE TABLE resource_class (
  id INTEGER PRIMARY KEY,
  resource_type_id INTEGER NOT NULL,
  value INTEGER NOT NULL,
  FOREIGN KEY (resource_type_id) REFERENCES resource_type (id)
);

CREATE TABLE component_class (
  id INTEGER PRIMARY KEY,
  parent_id INTEGER NOT NULL,
  FOREIGN KEY (parent_id) REFERENCES object (id)
);

CREATE TABLE body_class (
  id INTEGER PRIMARY KEY,
  health INTEGER NOT NULL,
  size INTEGER NOT NULL,
  height INTEGER NOT NULL,
  width INTEGER NOT NULL
 );

CREATE TABLE container_class (
  id INTEGER PRIMARY KEY,
  capacity INTEGER NOT NULL,
  has_forbidden_objects BOOLEAN NOT NULL
 );
 
 CREATE TABLE melee_class (
  id INTEGER PRIMARY KEY,
  range INTEGER NOT NULL,
  power INTEGER NOT NULL,
  damage_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (damage_type_id) REFERENCES damage_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id)
 );

 CREATE TABLE armor_class (
  id INTEGER PRIMARY KEY,
  durability INTEGER NOT NULL,
  armor_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (armor_type_id) REFERENCES armor_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id)
 );

 CREATE TABLE cannon_class (
  id INTEGER PRIMARY KEY,
  range INTEGER NOT NULL,
  power INTEGER NOT NULL,
  has_forbidden_projectiles BOOLEAN NOT NULL
 );

 CREATE TABLE projectile_class (
  id INTEGER PRIMARY KEY,
  damage INTEGER NOT NULL,
  damage_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (damage_type_id) REFERENCES damage_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id)
 );

 CREATE TABLE effect_class (
  id INTEGER PRIMARY KEY,
  duration INTEGER NOT NULL,
  rule TEXT NOT NULL
 );
 
 CREATE TABLE resource_type (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
 );

 CREATE TABLE damage_type (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
 );

 CREATE TABLE armor_type (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
 );

 CREATE TABLE element (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
 );

 CREATE TABLE material (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  strength INTEGER NOT NULL,
  texture TEXT NOT NULL
 );

 CREATE TABLE element_relation (
  strong_element INTEGER NOT NULL,
  weak_element INTEGER NOT NULL,
  PRIMARY KEY (strong_element,weak_element),
  FOREIGN KEY (strong_element) REFERENCES element (id),
  FOREIGN KEY (weak_element) REFERENCES element (id)
 );

 CREATE TABLE damage_type_armor_type (
  damage_type INTEGER NOT NULL,
  armor_type INTEGER NOT NULL,
  damage_effect INTEGER NOT NULL,
  PRIMARY KEY (damage_type,armor_type),
  FOREIGN KEY (damage_type) REFERENCES damage_type (id),
  FOREIGN KEY (armor_type) REFERENCES armor_type (id),
  FOREIGN KEY (damage_effect) REFERENCES damage_effect (id)
 );

 CREATE TABLE damage_type_material (
  damage_type INTEGER NOT NULL,
  material INTEGER NOT NULL,
  damage_effect INTEGER NOT NULL,
  PRIMARY KEY (damage_type,material),
  FOREIGN KEY (damage_type) REFERENCES damage_type (id),
  FOREIGN KEY (material) REFERENCES material (id),
  FOREIGN KEY (damage_effect) REFERENCES damage_effect (id)
 );

  CREATE TABLE element_material (
  element INTEGER NOT NULL,
  material INTEGER NOT NULL,
  damage_effect INTEGER NOT NULL,
  PRIMARY KEY (element,material),
  FOREIGN KEY (element) REFERENCES damage_type (id),
  FOREIGN KEY (material) REFERENCES material (id),
  FOREIGN KEY (damage_effect) REFERENCES damage_effect (id)
 );

 CREATE TABLE damage_effect(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
 );

 CREATE TABLE container_allowed_object (
  container_id INTEGER NOT NULL,
  allowed_object_id INTEGER NOT NULL,
  PRIMARY KEY (container_id,allowed_object_id),
  FOREIGN KEY (container_id) REFERENCES container_class (id),
  FOREIGN KEY (allowed_object_id) REFERENCES object_class (id)
 );

 CREATE TABLE cannon_allowed_projectiles (
  cannon_id INTEGER NOT NULL,
  allowed_projectile_id INTEGER NOT NULL,
  PRIMARY KEY (cannon_id,allowed_projectile_id),
  FOREIGN KEY (cannon_id) REFERENCES container_class (id),
  FOREIGN KEY (allowed_projectile_id) REFERENCES projectile_class (id)
 );

 CREATE TABLE object (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  play_area_id INTEGER NOT NULL,
  x INTEGER NOT NULL,
  y INTEGER NOT NULL,
  player_id INTEGER,
  interactible BOOLEAN NOT NULL,
  object_class_id INTEGER NOT NULL,
  has_children BOOLEAN NOT NULL,
  FOREIGN KEY (play_area_id,x,y) REFERENCES cell (play_area_id,x,y),
  FOREIGN KEY (object_class_id) REFERENCES object_class (id),
  FOREIGN KEY (player_id) REFERENCES user (id)
 );

 CREATE TABLE unit (
  id INTEGER PRIMARY KEY,
  speed INTEGER NOT NULL,
  range INTEGER NOT NULL,
  defense INTEGER NOT NULL,
  attack INTEGER NOT NULL,
  FOREIGN KEY (id) REFERENCES object (id)
);

CREATE TABLE factory (
  id INTEGER PRIMARY KEY,
  cool_down INTEGER NOT NULL,
  batch_size INTEGER NOT NULL,
  FOREIGN KEY (id) REFERENCES object (id)
);

CREATE TABLE factory_cost (
  factory_id INTEGER PRIMARY KEY,
  resource_type_id INTEGER NOT NULL,
  cost INTEGER NOT NULL,
  FOREIGN KEY (factory_id) REFERENCES factory (id),
  FOREIGN KEY (resource_type_id) REFERENCES resource_type (id)
);

CREATE TABLE resource (
  id INTEGER PRIMARY KEY,
  resource_type_id INTEGER NOT NULL,
  value INTEGER NOT NULL,
  FOREIGN KEY (resource_type_id) REFERENCES resource_type (id),
  FOREIGN KEY (id) REFERENCES object (id)
);

CREATE TABLE component (
  id INTEGER PRIMARY KEY,
  parent_id INTEGER NOT NULL,
  FOREIGN KEY (parent_id) REFERENCES object (id),
  FOREIGN KEY (id) REFERENCES object (id)
);

CREATE TABLE body (
  id INTEGER PRIMARY KEY,
  health INTEGER NOT NULL,
  size INTEGER NOT NULL,
  height INTEGER NOT NULL,
  width INTEGER NOT NULL,
  FOREIGN KEY (id) REFERENCES object (id)
 );

CREATE TABLE container (
  id INTEGER PRIMARY KEY,
  capacity INTEGER NOT NULL,
  has_forbidden_objects BOOLEAN NOT NULL,
  FOREIGN KEY (id) REFERENCES object (id)
 );
 
 CREATE TABLE melee (
  id INTEGER PRIMARY KEY,
  range INTEGER NOT NULL,
  power INTEGER NOT NULL,
  damage_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (damage_type_id) REFERENCES damage_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id),
  FOREIGN KEY (id) REFERENCES object (id)
 );

 CREATE TABLE armor (
  id INTEGER PRIMARY KEY,
  durability INTEGER NOT NULL,
  armor_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (armor_type_id) REFERENCES armor_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id),
  FOREIGN KEY (id) REFERENCES object (id)
 );

 CREATE TABLE cannon (
  id INTEGER PRIMARY KEY,
  range INTEGER NOT NULL,
  power INTEGER NOT NULL,
  has_forbidden_projectiles BOOLEAN NOT NULL,
  FOREIGN KEY (id) REFERENCES object (id)
 );

 CREATE TABLE projectile (
  id INTEGER PRIMARY KEY,
  damage INTEGER NOT NULL,
  damage_type_id INTEGER NOT NULL,
  element_id INTEGER NOT NULL,
  FOREIGN KEY (damage_type_id) REFERENCES damage_type (id),
  FOREIGN KEY (element_id) REFERENCES element (id),
  FOREIGN KEY (id) REFERENCES object (id)
 );

 CREATE TABLE turn (
  game_id INTEGER NOT NULL,
  id INTEGER NOT NULL, 
  player_id INTEGER,
  PRIMARY KEY (game_id,id),
  FOREIGN KEY (game_id) REFERENCES game (id),
  FOREIGN KEY (player_id) REFERENCES user (id)
 );

 CREATE TABLE move (
  game_id INTEGER NOT NULL,
  turn_id INTEGER NOT NULL,
  id INTEGER NOT NULL,
  player_id INTEGER,
  object_id INTEGER,
  action_type_id INTEGER NOT NULL,
  target_cell_id INTEGER NOT NULL,
  target_object_id INTEGER NOT NULL,
  PRIMARY KEY (game_id,turn_id,id),
  FOREIGN KEY (game_id) REFERENCES game (id),
  FOREIGN KEY (turn_id) REFERENCES turn (id),
  FOREIGN KEY (player_id) REFERENCES user (id)
 );

 CREATE TABLE action_type (
  id INTEGER NOT NULL,
  name TEXT NOT NULL
 );

 CREATE TABLE cell_state (
  id INTEGER NOT NULL,
  name TEXT NOT NULL
 );
