definitions{
	characteristics{
		meter
	}
	types{
		integer 	default
		text
		real
		datetime
	}
}

datamodel boardgame{

	entities{

		player{
			name text

		}

		game{
			name text
			created datetime
		}
		
		board{
			name text
		}

		play_area_template{
			name text	
		}

		play_area{
			name text
		}

		cell_template{
			x integer
			y integer
		}

		cell{
			x integer
			y integer
		}

		terrain_type{
			name text
			speed integer
		}

		object_class{
			name text
			interactible boolean
			has_children boolean
		}

		unit_class{
			name text
		}

		factory_class{
			cool_down integer
			batch_size integer
		}

		resource_type{
			name text
		}

		#hierarchy? redo as relation?
		component_class{
			
		}

		body_class{
			health integer
			size integer
			height integer
			width intger
		}
		
		container_class{
			capacity integer
			allow_all boolean
			allow_none boolean
		}


	}

	relations{
		
		:game
		-board
		
		:game
		:player

		:factory_class:resource_type{cost integer}

	}

}
