from sqlalchemy import *
from sqlalchemy.orm import (scoped_session, sessionmaker, relationship,backref)
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///database.sqlite3', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))

Base = declarative_base()
# We will need this for querying
Base.query = db_session.query_property()

class Game_player(Base):
	__tablename__ = "game_player"
	game_id = Column(Integer,primary_key=True)
	player_id = Column(Integer,primary_key=True)
	
	game_id = Column(Integer,ForeignKey('game.id'))
	player_id = Column(Integer,ForeignKey('player.id'))
	game = relationship(
		Game,
		backref=backref("game_player_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?
	player = relationship(
		Player,
		backref=backref("game_player_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Factory_class_resource_type(Base):
	__tablename__ = "factory_class_resource_type"
	factory_class_id = Column(Integer,primary_key=True)
	resource_type_id = Column(Integer,primary_key=True)
	cost = Column(Integer)
	factory_class_id = Column(Integer,ForeignKey('factory_class.id'))
	resource_type_id = Column(Integer,ForeignKey('resource_type.id'))
	factory_class = relationship(
		Factory_class,
		backref=backref("factory_class_resource_type_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?
	resource_type = relationship(
		Resource_type,
		backref=backref("factory_class_resource_type_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Player(Base):
	__tablename__ = "player"
	id = Column(Integer,primary_key=True)
	name = Column(String)

	game_set = relationship(
		Game,
		backref=backref("player_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Game(Base):
	__tablename__ = "game"
	id = Column(Integer,primary_key=True)
	name = Column(String)
	created = Column()
	board_id = Column(Integer,ForeignKey('board.id'))
	board = relationship(
		Board,
		backref=backref("game_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?
	player_set = relationship(
		Player,
		backref=backref("game_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Board(Base):
	__tablename__ = "board"
	id = Column(Integer,primary_key=True)
	name = Column(String)

	game_set = relationship(
		Game,
		backref=backref("board"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Play_area_template(Base):
	__tablename__ = "play_area_template"
	id = Column(Integer,primary_key=True)
	name = Column(String)



class Play_area(Base):
	__tablename__ = "play_area"
	id = Column(Integer,primary_key=True)
	name = Column(String)



class Cell_template(Base):
	__tablename__ = "cell_template"
	id = Column(Integer,primary_key=True)
	x = Column(Integer)
	y = Column(Integer)



class Cell(Base):
	__tablename__ = "cell"
	id = Column(Integer,primary_key=True)
	x = Column(Integer)
	y = Column(Integer)



class Terrain_type(Base):
	__tablename__ = "terrain_type"
	id = Column(Integer,primary_key=True)
	name = Column(String)
	speed = Column(Integer)



class Object_class(Base):
	__tablename__ = "object_class"
	id = Column(Integer,primary_key=True)
	name = Column(String)
	interactible = Column()
	has_children = Column()



class Unit_class(Base):
	__tablename__ = "unit_class"
	id = Column(Integer,primary_key=True)
	name = Column(String)



class Factory_class(Base):
	__tablename__ = "factory_class"
	id = Column(Integer,primary_key=True)
	cool_down = Column(Integer)
	batch_size = Column(Integer)

	resource_type_set = relationship(
		Resource_type,
		backref=backref("factory_class_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Resource_type(Base):
	__tablename__ = "resource_type"
	id = Column(Integer,primary_key=True)
	name = Column(String)

	factory_class_set = relationship(
		Factory_class,
		backref=backref("resource_type_set"
			uselist=True,
			cascade='delete,all')
		)
		#TODO:Do we want a cascade delete?

class Component_class(Base):
	__tablename__ = "component_class"
	id = Column(Integer,primary_key=True)
	



class Body_class(Base):
	__tablename__ = "body_class"
	id = Column(Integer,primary_key=True)
	health = Column(Integer)
	size = Column(Integer)
	height = Column(Integer)
	width = Column()



class Container_class(Base):
	__tablename__ = "container_class"
	id = Column(Integer,primary_key=True)
	capacity = Column(Integer)
	allow_all = Column()
	allow_none = Column()

