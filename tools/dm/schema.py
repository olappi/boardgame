from sqlalchemy import *
from sqlalchemy.orm import (scoped_session, sessionmaker, relationship,backref)
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///boardgame.sqlite3', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))

Base = declarative_base()
# We will need this for querying
Base.query = db_session.query_property()

class Game_player(Base):
	__tablename__ = "game_player"
	player_id = Column(Integer, ForeignKey('player.id'), primary_key=True)
	game_id = Column(Integer, ForeignKey('game.id'), primary_key=True)

	game = relationship("Game", back_populates="player_set")
	player = relationship("Player", back_populates="game_set")


class Factory_class_resource_type(Base):
	__tablename__ = "factory_class_resource_type"
	resource_type_id = Column(Integer, ForeignKey('resource_type.id'), primary_key=True)
	factory_class_id = Column(Integer, ForeignKey('factory_class.id'), primary_key=True)
	cost = Column(Integer)

	factory_class = relationship("Factory_class", back_populates="resource_type_set")
	resource_type = relationship("Resource_type", back_populates="factory_class_set")

class Player(Base):
	__tablename__ = "player"
	id = Column(Integer, primary_key=True)
	name = Column(String)
	game_set = relationship("Game_player", back_populates = "player")


class Game(Base):
	__tablename__ = "game"
	id = Column(Integer, primary_key=True)
	name = Column(String)
	created = Column(DateTime)
	board_id = Column(Integer, ForeignKey('board.id'))

	board = relationship("Board", back_populates = "game_set")
		#TODO:Do we want a cascade delete?

	player_set = relationship("Game_player", back_populates = "game")


class Board(Base):
	__tablename__ = "board"
	id = Column(Integer, primary_key=True)
	name = Column(String)

	game_set = relationship("Game", back_populates = "board")
		#TODO:Do we want a cascade delete?


class Play_area_template(Base):
	__tablename__ = "play_area_template"
	id = Column(Integer, primary_key=True)
	name = Column(String)


class Play_area(Base):
	__tablename__ = "play_area"
	id = Column(Integer, primary_key=True)
	name = Column(String)


class Cell_template(Base):
	__tablename__ = "cell_template"
	id = Column(Integer, primary_key=True)
	x = Column(Integer)
	y = Column(Integer)


class Cell(Base):
	__tablename__ = "cell"
	id = Column(Integer, primary_key=True)
	x = Column(Integer)
	y = Column(Integer)


class Terrain_type(Base):
	__tablename__ = "terrain_type"
	id = Column(Integer, primary_key=True)
	name = Column(String)
	speed = Column(Integer)


class Object_class(Base):
	__tablename__ = "object_class"
	id = Column(Integer, primary_key=True)
	name = Column(String)
	interactible = Column()
	has_children = Column()


class Unit_class(Base):
	__tablename__ = "unit_class"
	id = Column(Integer, primary_key=True)
	name = Column(String)


class Factory_class(Base):
	__tablename__ = "factory_class"
	id = Column(Integer, primary_key=True)
	cool_down = Column(Integer)
	batch_size = Column(Integer)
	resource_type_set = relationship("Factory_class_resource_type", back_populates = "factory_class")


class Resource_type(Base):
	__tablename__ = "resource_type"
	id = Column(Integer, primary_key=True)
	name = Column(String)
	factory_class_set = relationship("Factory_class_resource_type", back_populates = "resource_type")


class Component_class(Base):
	__tablename__ = "component_class"
	id = Column(Integer, primary_key=True)


class Body_class(Base):
	__tablename__ = "body_class"
	id = Column(Integer, primary_key=True)
	health = Column(Integer)
	size = Column(Integer)
	height = Column(Integer)
	width = Column()


class Container_class(Base):
	__tablename__ = "container_class"
	id = Column(Integer, primary_key=True)
	capacity = Column(Integer)
	allow_all = Column()
	allow_none = Column()
