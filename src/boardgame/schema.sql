DROP TABLE IF EXISTS player;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS board;
DROP TABLE IF EXISTS play_area_template;
DROP TABLE IF EXISTS play_area;
DROP TABLE IF EXISTS cell_template;
DROP TABLE IF EXISTS cell;
DROP TABLE IF EXISTS terrain_type;
DROP TABLE IF EXISTS object_class;
DROP TABLE IF EXISTS unit_class;
DROP TABLE IF EXISTS factory_class;
DROP TABLE IF EXISTS resource_type;
DROP TABLE IF EXISTS component_class;
DROP TABLE IF EXISTS body_class;
DROP TABLE IF EXISTS container_class;
DROP TABLE IF EXISTS factory_class_resource_type;

CREATE TABLE player(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE game(
	id INTEGER NOT NULL,
	name TEXT,
	created INTEGER,
	board_id INTEGER,
	player1_id INTEGER,
	player2_id INTEGER,
	FOREIGN KEY (board_id) REFERENCES board (id),
	FOREIGN KEY (player1_id) REFERENCES player (id),
	FOREIGN KEY (player2_id) REFERENCES player (id),
	PRIMARY KEY (id)
);
CREATE TABLE board(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE play_area_template(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE play_area(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE cell_template(
	id INTEGER NOT NULL,
	x INTEGER,
	y INTEGER,
	PRIMARY KEY (id)
);
CREATE TABLE cell(
	id INTEGER NOT NULL,
	x INTEGER,
	y INTEGER,
	PRIMARY KEY (id)
);
CREATE TABLE terrain_type(
	id INTEGER NOT NULL,
	name TEXT,
	speed INTEGER,
	PRIMARY KEY (id)
);
CREATE TABLE object_class(
	id INTEGER NOT NULL,
	name TEXT,
	interactible ,
	has_children ,
	PRIMARY KEY (id)
);
CREATE TABLE unit_class(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE factory_class(
	id INTEGER NOT NULL,
	cool_down INTEGER,
	batch_size INTEGER,
	PRIMARY KEY (id)
);
CREATE TABLE resource_type(
	id INTEGER NOT NULL,
	name TEXT,
	PRIMARY KEY (id)
);
CREATE TABLE component_class(
	id INTEGER NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE body_class(
	id INTEGER NOT NULL,
	health INTEGER,
	size INTEGER,
	height INTEGER,
	width ,
	PRIMARY KEY (id)
);
CREATE TABLE container_class(
	id INTEGER NOT NULL,
	capacity INTEGER,
	allow_all ,
	allow_none ,
	PRIMARY KEY (id)
);
CREATE TABLE factory_class_resource_type(
	factory_class_id INTEGER NOT NULL,
	resource_type_id INTEGER NOT NULL,
	cost INTEGER,
	FOREIGN KEY (factory_class_id) REFERENCES factory_class (id),
	FOREIGN KEY (resource_type_id) REFERENCES resource_type (id),
	PRIMARY KEY (factory_class_id,resource_type_id)
);