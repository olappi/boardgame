import json
from .db import get_db


class Entity:
	written = False
	commited = False

	def __init__(self,field_tuple=None,json_data=None,pk_tuple=None):
		
		self.data = {}
		if field_tuple:
			self.from_tuple(field_tuple)
		elif json_data:
			self.from_json(json_data)
		elif pk_tuple:
			self.from_id(pk_tuple)
		else:
			raise Exception("No input data given")


	def to_json(self):
		return json.dumps(self.data)

	def to_tuple(self):
		return self.data_tuple
	
	def commit(self,db):
		db.commit()
		self.commited = True

class player(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM player WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No player with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM player WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE player SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO player (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
	def related_game(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["id"],)
		cursor.execute("SELECT * FROM game WHERE player1_id = ?;",query_tuple)

	def related_game(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["id"],)
		cursor.execute("SELECT * FROM game WHERE player2_id = ?;",query_tuple)


class game(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"],self.data["created"],self.data["board_id"],self.data["player1_id"],self.data["player2_id"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.data["created"] = data["created"] if "created" in data else None
		self.data["board_id"] = data["board_id"] if "board_id" in data else None
		self.data["player1_id"] = data["player1_id"] if "player1_id" in data else None
		self.data["player2_id"] = data["player2_id"] if "player2_id" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM game WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No game with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.non_pk_fields["created"] = self.data["created"]
		self.non_pk_fields["board_id"] = self.data["board_id"]
		self.non_pk_fields["player1_id"] = self.data["player1_id"]
		self.non_pk_fields["player2_id"] = self.data["player2_id"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.data["created"] = data["created"] if "created" in data else None
		self.data["board_id"] = data["board_id"] if "board_id" in data else None
		self.data["player1_id"] = data["player1_id"] if "player1_id" in data else None
		self.data["player2_id"] = data["player2_id"] if "player2_id" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM game WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE game SET (name,created,board_id,player1_id,player2_id) = (?,?,?,?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO game (name,created,board_id,player1_id,player2_id) VALUES (?,?,?,?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
	def related_board(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["board_id"],)
		cursor.execute("SELECT * FROM board WHERE id = ?;",query_tuple)

	def related_player1(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["player1_id"],)
		cursor.execute("SELECT * FROM player WHERE id = ?;",query_tuple)

	def related_player2(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["player2_id"],)
		cursor.execute("SELECT * FROM player WHERE id = ?;",query_tuple)


class board(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM board WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No board with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM board WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE board SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO board (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
	def related_game(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["id"],)
		cursor.execute("SELECT * FROM game WHERE board_id = ?;",query_tuple)


class play_area_template(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM play_area_template WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No play_area_template with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM play_area_template WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE play_area_template SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO play_area_template (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class play_area(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM play_area WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No play_area with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM play_area WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE play_area SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO play_area (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class cell_template(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["x"],self.data["y"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["x"] = data["x"] if "x" in data else None
		self.data["y"] = data["y"] if "y" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM cell_template WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No cell_template with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["x"] = self.data["x"]
		self.non_pk_fields["y"] = self.data["y"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["x"] = data["x"] if "x" in data else None
		self.data["y"] = data["y"] if "y" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM cell_template WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE cell_template SET (x,y) = (?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO cell_template (x,y) VALUES (?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class cell(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["x"],self.data["y"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["x"] = data["x"] if "x" in data else None
		self.data["y"] = data["y"] if "y" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM cell WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No cell with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["x"] = self.data["x"]
		self.non_pk_fields["y"] = self.data["y"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["x"] = data["x"] if "x" in data else None
		self.data["y"] = data["y"] if "y" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM cell WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE cell SET (x,y) = (?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO cell (x,y) VALUES (?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class terrain_type(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"],self.data["speed"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.data["speed"] = data["speed"] if "speed" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM terrain_type WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No terrain_type with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.non_pk_fields["speed"] = self.data["speed"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.data["speed"] = data["speed"] if "speed" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM terrain_type WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE terrain_type SET (name,speed) = (?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO terrain_type (name,speed) VALUES (?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class object_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"],self.data["interactible"],self.data["has_children"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.data["interactible"] = data["interactible"] if "interactible" in data else None
		self.data["has_children"] = data["has_children"] if "has_children" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM object_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No object_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.non_pk_fields["interactible"] = self.data["interactible"]
		self.non_pk_fields["has_children"] = self.data["has_children"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.data["interactible"] = data["interactible"] if "interactible" in data else None
		self.data["has_children"] = data["has_children"] if "has_children" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM object_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE object_class SET (name,interactible,has_children) = (?,?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO object_class (name,interactible,has_children) VALUES (?,?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class unit_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM unit_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No unit_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM unit_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE unit_class SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO unit_class (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class factory_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["cool_down"],self.data["batch_size"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["cool_down"] = data["cool_down"] if "cool_down" in data else None
		self.data["batch_size"] = data["batch_size"] if "batch_size" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM factory_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No factory_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["cool_down"] = self.data["cool_down"]
		self.non_pk_fields["batch_size"] = self.data["batch_size"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["cool_down"] = data["cool_down"] if "cool_down" in data else None
		self.data["batch_size"] = data["batch_size"] if "batch_size" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM factory_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE factory_class SET (cool_down,batch_size) = (?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO factory_class (cool_down,batch_size) VALUES (?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
	def related_resource_type(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["id"],)
		
		cursor.execute("SELECT resource_type .* FROM factory_class_resource_type JOIN resource_type ON factory_class_resource_type.resource_type_id = resource_type.id WHERE factory_class_resource_type.factory_class_id = ?;",query_tuple)
		return [resource_type(e) for e in cursor.fetchall()]


class resource_type(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["name"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM resource_type WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No resource_type with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["name"] = self.data["name"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["name"] = data["name"] if "name" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM resource_type WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE resource_type SET (name) = (?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO resource_type (name) VALUES (?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
	def related_factory_class(self):
		db = get_db()
		cursor = db.cursor()
		query_tuple = (self.data["id"],)
		
		cursor.execute("SELECT factory_class .* FROM factory_class_resource_type JOIN factory_class ON factory_class_resource_type.factory_class_id = factory_class.id WHERE factory_class_resource_type.resource_type_id = ?;",query_tuple)
		return [factory_class(e) for e in cursor.fetchall()]


class component_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM component_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No component_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return

		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM component_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE component_class SET () = () WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO component_class () VALUES ();",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class body_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["health"],self.data["size"],self.data["height"],self.data["width"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["health"] = data["health"] if "health" in data else None
		self.data["size"] = data["size"] if "size" in data else None
		self.data["height"] = data["height"] if "height" in data else None
		self.data["width"] = data["width"] if "width" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM body_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No body_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["health"] = self.data["health"]
		self.non_pk_fields["size"] = self.data["size"]
		self.non_pk_fields["height"] = self.data["height"]
		self.non_pk_fields["width"] = self.data["width"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["health"] = data["health"] if "health" in data else None
		self.data["size"] = data["size"] if "size" in data else None
		self.data["height"] = data["height"] if "height" in data else None
		self.data["width"] = data["width"] if "width" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM body_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE body_class SET (health,size,height,width) = (?,?,?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO body_class (health,size,height,width) VALUES (?,?,?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			



class container_class(Entity):
	def from_tuple(self,field_tuple):
		self.data["id"],self.data["capacity"],self.data["allow_all"],self.data["allow_none"] = field_tuple
		self.__data_refresh__()
		self.written = True
		self.commited = True
	
	def from_json(self,data):
		self.data["id"] = data["id"] if "id" in data else None
		self.data["capacity"] = data["capacity"] if "capacity" in data else None
		self.data["allow_all"] = data["allow_all"] if "allow_all" in data else None
		self.data["allow_none"] = data["allow_none"] if "allow_none" in data else None
		self.__data_refresh__()

	def from_id(self,pk_tuple):
		db = get_db()
		cursor = db.cursor()
		cursor.execute("SELECT * FROM container_class WHERE id = ?;",pk_tuple)
		row = cursor.fetchone()
		if row == None:
			raise Exception("Error: No container_class with PK = {}".format(str(pk_tuple)))
		self.from_tuple(row)

	def __data_refresh__(self):
		self.pk_fields = {}
		self.pk_fields["id"] = self.data["id"]
		self.non_pk_fields = {}
		self.non_pk_fields["capacity"] = self.data["capacity"]
		self.non_pk_fields["allow_all"] = self.data["allow_all"]
		self.non_pk_fields["allow_none"] = self.data["allow_none"]
		self.pk_tuple = tuple([v for k,v in self.pk_fields.items()])
		self.non_pk_tuple = tuple([v for k,v in self.non_pk_fields.items()])
		self.data_tuple = tuple([v for k,v in self.data.items()])

	def change(self,data):

		self.written = False
		if not data:
			return
		self.data["capacity"] = data["capacity"] if "capacity" in data else None
		self.data["allow_all"] = data["allow_all"] if "allow_all" in data else None
		self.data["allow_none"] = data["allow_none"] if "allow_none" in data else None
		self.__data_refresh__()
	


	def delete(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()
		
		if self.data["id"]:
			cursor.execute("DELETE FROM container_class WHERE id = ?;",self.pk_tuple)

		if commit:
			self.commit(db)

	def save(self,db=None,commit=False):
		if not db:
			db = get_db()
		cursor = db.cursor()

		if self.data["id"]:
			cursor.execute("UPDATE container_class SET (capacity,allow_all,allow_none) = (?,?,?) WHERE id = ?;",self.non_pk_tuple+self.pk_tuple)
		else:
			cursor.execute("INSERT INTO container_class (capacity,allow_all,allow_none) VALUES (?,?,?);",self.non_pk_tuple)
		self.written = True
		self.commited = False
		
		if commit:
			self.commit(db)
			
