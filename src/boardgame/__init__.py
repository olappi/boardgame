import os
import sys
from flask import Flask,request
from .db import get_db
from . import entities
import json


def create_app(test_config=None):
	# create and configure the app
	app = Flask(__name__, instance_relative_config=True)
	app.config.from_mapping(
		SECRET_KEY='dev',
		DATABASE=os.path.join(app.instance_path, 'boardgame.sqlite'),
	)

	if test_config is None:
		# load the instance config, if it exists, when not testing
		app.config.from_pyfile('config.py', silent=True)
	else:
		# load the test config if passed in
		app.config.from_mapping(test_config)

	# ensure the instance folder exists
	try:
		os.makedirs(app.instance_path)
	except OSError as e:
		print(e)


	@app.route('/')
	def schema():
		return '''{ 
		"player":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"game":{
			"fields":{
				"id":"integer",
				"name":"text",
				"created":"datetime",
				"board_id":"integer",
				"player1_id":"integer",
				"player2_id":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"board":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"play_area_template":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"play_area":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"cell_template":{
			"fields":{
				"id":"integer",
				"x":"integer",
				"y":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"cell":{
			"fields":{
				"id":"integer",
				"x":"integer",
				"y":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"terrain_type":{
			"fields":{
				"id":"integer",
				"name":"text",
				"speed":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"object_class":{
			"fields":{
				"id":"integer",
				"name":"text",
				"interactible":"boolean",
				"has_children":"boolean"
			},
			"key":{
				"id":"integer"
			}
		},
		"unit_class":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"factory_class":{
			"fields":{
				"id":"integer",
				"cool_down":"integer",
				"batch_size":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"resource_type":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"component_class":{
			"fields":{
				"id":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"body_class":{
			"fields":{
				"id":"integer",
				"health":"integer",
				"size":"integer",
				"height":"integer",
				"width":"intger"
			},
			"key":{
				"id":"integer"
			}
		},
		"container_class":{
			"fields":{
				"id":"integer",
				"capacity":"integer",
				"allow_all":"boolean",
				"allow_none":"boolean"
			},
			"key":{
				"id":"integer"
			}
		} 
		}'''

	@app.route('/player/',methods=['GET','POST'])
	def player_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM player')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_player = entities.player(json_data = data)
				o_player.save(commit = True )
				return "Successfully created player"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/player/<id>',methods=['GET','PATCH','DELETE'])
	def player(id):

		try:
			if request.method == 'GET':
	
				o_player = entities.player(pk_tuple=(id,))
				return o_player.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_player =entities.player(pk_tuple =(id,))
				o_player.change(data=data)
				o_player.save(commit=True)
				return "Successfully updated player, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_player = entities.player(pk_tuple=(id,))
				o_player.delete(commit=True)
				return "Successfully deleted player, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/game/',methods=['GET','POST'])
	def game_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM game')
				response = [ {"id":row["id"],"name":row["name"],"created":row["created"],"board_id":row["board_id"],"player1_id":row["player1_id"],"player2_id":row["player2_id"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_game = entities.game(json_data = data)
				o_game.save(commit = True )
				return "Successfully created game"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/game/<id>',methods=['GET','PATCH','DELETE'])
	def game(id):

		try:
			if request.method == 'GET':
	
				o_game = entities.game(pk_tuple=(id,))
				return o_game.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_game =entities.game(pk_tuple =(id,))
				o_game.change(data=data)
				o_game.save(commit=True)
				return "Successfully updated game, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_game = entities.game(pk_tuple=(id,))
				o_game.delete(commit=True)
				return "Successfully deleted game, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/board/',methods=['GET','POST'])
	def board_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM board')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_board = entities.board(json_data = data)
				o_board.save(commit = True )
				return "Successfully created board"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/board/<id>',methods=['GET','PATCH','DELETE'])
	def board(id):

		try:
			if request.method == 'GET':
	
				o_board = entities.board(pk_tuple=(id,))
				return o_board.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_board =entities.board(pk_tuple =(id,))
				o_board.change(data=data)
				o_board.save(commit=True)
				return "Successfully updated board, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_board = entities.board(pk_tuple=(id,))
				o_board.delete(commit=True)
				return "Successfully deleted board, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/play_area_template/',methods=['GET','POST'])
	def play_area_template_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM play_area_template')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_play_area_template = entities.play_area_template(json_data = data)
				o_play_area_template.save(commit = True )
				return "Successfully created play_area_template"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/play_area_template/<id>',methods=['GET','PATCH','DELETE'])
	def play_area_template(id):

		try:
			if request.method == 'GET':
	
				o_play_area_template = entities.play_area_template(pk_tuple=(id,))
				return o_play_area_template.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_play_area_template =entities.play_area_template(pk_tuple =(id,))
				o_play_area_template.change(data=data)
				o_play_area_template.save(commit=True)
				return "Successfully updated play_area_template, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_play_area_template = entities.play_area_template(pk_tuple=(id,))
				o_play_area_template.delete(commit=True)
				return "Successfully deleted play_area_template, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/play_area/',methods=['GET','POST'])
	def play_area_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM play_area')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_play_area = entities.play_area(json_data = data)
				o_play_area.save(commit = True )
				return "Successfully created play_area"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/play_area/<id>',methods=['GET','PATCH','DELETE'])
	def play_area(id):

		try:
			if request.method == 'GET':
	
				o_play_area = entities.play_area(pk_tuple=(id,))
				return o_play_area.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_play_area =entities.play_area(pk_tuple =(id,))
				o_play_area.change(data=data)
				o_play_area.save(commit=True)
				return "Successfully updated play_area, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_play_area = entities.play_area(pk_tuple=(id,))
				o_play_area.delete(commit=True)
				return "Successfully deleted play_area, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/cell_template/',methods=['GET','POST'])
	def cell_template_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM cell_template')
				response = [ {"id":row["id"],"x":row["x"],"y":row["y"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_cell_template = entities.cell_template(json_data = data)
				o_cell_template.save(commit = True )
				return "Successfully created cell_template"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/cell_template/<id>',methods=['GET','PATCH','DELETE'])
	def cell_template(id):

		try:
			if request.method == 'GET':
	
				o_cell_template = entities.cell_template(pk_tuple=(id,))
				return o_cell_template.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_cell_template =entities.cell_template(pk_tuple =(id,))
				o_cell_template.change(data=data)
				o_cell_template.save(commit=True)
				return "Successfully updated cell_template, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_cell_template = entities.cell_template(pk_tuple=(id,))
				o_cell_template.delete(commit=True)
				return "Successfully deleted cell_template, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/cell/',methods=['GET','POST'])
	def cell_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM cell')
				response = [ {"id":row["id"],"x":row["x"],"y":row["y"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_cell = entities.cell(json_data = data)
				o_cell.save(commit = True )
				return "Successfully created cell"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/cell/<id>',methods=['GET','PATCH','DELETE'])
	def cell(id):

		try:
			if request.method == 'GET':
	
				o_cell = entities.cell(pk_tuple=(id,))
				return o_cell.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_cell =entities.cell(pk_tuple =(id,))
				o_cell.change(data=data)
				o_cell.save(commit=True)
				return "Successfully updated cell, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_cell = entities.cell(pk_tuple=(id,))
				o_cell.delete(commit=True)
				return "Successfully deleted cell, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/terrain_type/',methods=['GET','POST'])
	def terrain_type_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM terrain_type')
				response = [ {"id":row["id"],"name":row["name"],"speed":row["speed"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_terrain_type = entities.terrain_type(json_data = data)
				o_terrain_type.save(commit = True )
				return "Successfully created terrain_type"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/terrain_type/<id>',methods=['GET','PATCH','DELETE'])
	def terrain_type(id):

		try:
			if request.method == 'GET':
	
				o_terrain_type = entities.terrain_type(pk_tuple=(id,))
				return o_terrain_type.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_terrain_type =entities.terrain_type(pk_tuple =(id,))
				o_terrain_type.change(data=data)
				o_terrain_type.save(commit=True)
				return "Successfully updated terrain_type, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_terrain_type = entities.terrain_type(pk_tuple=(id,))
				o_terrain_type.delete(commit=True)
				return "Successfully deleted terrain_type, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/object_class/',methods=['GET','POST'])
	def object_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM object_class')
				response = [ {"id":row["id"],"name":row["name"],"interactible":row["interactible"],"has_children":row["has_children"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_object_class = entities.object_class(json_data = data)
				o_object_class.save(commit = True )
				return "Successfully created object_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/object_class/<id>',methods=['GET','PATCH','DELETE'])
	def object_class(id):

		try:
			if request.method == 'GET':
	
				o_object_class = entities.object_class(pk_tuple=(id,))
				return o_object_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_object_class =entities.object_class(pk_tuple =(id,))
				o_object_class.change(data=data)
				o_object_class.save(commit=True)
				return "Successfully updated object_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_object_class = entities.object_class(pk_tuple=(id,))
				o_object_class.delete(commit=True)
				return "Successfully deleted object_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/unit_class/',methods=['GET','POST'])
	def unit_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM unit_class')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_unit_class = entities.unit_class(json_data = data)
				o_unit_class.save(commit = True )
				return "Successfully created unit_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/unit_class/<id>',methods=['GET','PATCH','DELETE'])
	def unit_class(id):

		try:
			if request.method == 'GET':
	
				o_unit_class = entities.unit_class(pk_tuple=(id,))
				return o_unit_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_unit_class =entities.unit_class(pk_tuple =(id,))
				o_unit_class.change(data=data)
				o_unit_class.save(commit=True)
				return "Successfully updated unit_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_unit_class = entities.unit_class(pk_tuple=(id,))
				o_unit_class.delete(commit=True)
				return "Successfully deleted unit_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/factory_class/',methods=['GET','POST'])
	def factory_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM factory_class')
				response = [ {"id":row["id"],"cool_down":row["cool_down"],"batch_size":row["batch_size"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_factory_class = entities.factory_class(json_data = data)
				o_factory_class.save(commit = True )
				return "Successfully created factory_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/factory_class/<id>',methods=['GET','PATCH','DELETE'])
	def factory_class(id):

		try:
			if request.method == 'GET':
	
				o_factory_class = entities.factory_class(pk_tuple=(id,))
				return o_factory_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_factory_class =entities.factory_class(pk_tuple =(id,))
				o_factory_class.change(data=data)
				o_factory_class.save(commit=True)
				return "Successfully updated factory_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_factory_class = entities.factory_class(pk_tuple=(id,))
				o_factory_class.delete(commit=True)
				return "Successfully deleted factory_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/resource_type/',methods=['GET','POST'])
	def resource_type_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM resource_type')
				response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_resource_type = entities.resource_type(json_data = data)
				o_resource_type.save(commit = True )
				return "Successfully created resource_type"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/resource_type/<id>',methods=['GET','PATCH','DELETE'])
	def resource_type(id):

		try:
			if request.method == 'GET':
	
				o_resource_type = entities.resource_type(pk_tuple=(id,))
				return o_resource_type.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_resource_type =entities.resource_type(pk_tuple =(id,))
				o_resource_type.change(data=data)
				o_resource_type.save(commit=True)
				return "Successfully updated resource_type, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_resource_type = entities.resource_type(pk_tuple=(id,))
				o_resource_type.delete(commit=True)
				return "Successfully deleted resource_type, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/component_class/',methods=['GET','POST'])
	def component_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM component_class')
				response = [ {"id":row["id"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_component_class = entities.component_class(json_data = data)
				o_component_class.save(commit = True )
				return "Successfully created component_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/component_class/<id>',methods=['GET','PATCH','DELETE'])
	def component_class(id):

		try:
			if request.method == 'GET':
	
				o_component_class = entities.component_class(pk_tuple=(id,))
				return o_component_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_component_class =entities.component_class(pk_tuple =(id,))
				o_component_class.change(data=data)
				o_component_class.save(commit=True)
				return "Successfully updated component_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_component_class = entities.component_class(pk_tuple=(id,))
				o_component_class.delete(commit=True)
				return "Successfully deleted component_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/body_class/',methods=['GET','POST'])
	def body_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM body_class')
				response = [ {"id":row["id"],"health":row["health"],"size":row["size"],"height":row["height"],"width":row["width"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_body_class = entities.body_class(json_data = data)
				o_body_class.save(commit = True )
				return "Successfully created body_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/body_class/<id>',methods=['GET','PATCH','DELETE'])
	def body_class(id):

		try:
			if request.method == 'GET':
	
				o_body_class = entities.body_class(pk_tuple=(id,))
				return o_body_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_body_class =entities.body_class(pk_tuple =(id,))
				o_body_class.change(data=data)
				o_body_class.save(commit=True)
				return "Successfully updated body_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_body_class = entities.body_class(pk_tuple=(id,))
				o_body_class.delete(commit=True)
				return "Successfully deleted body_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	@app.route('/container_class/',methods=['GET','POST'])
	def container_class_collection():
		try:
			if request.method == 'GET':
				db = get_db()
				cursor = db.cursor()
				cursor.execute('SELECT * FROM container_class')
				response = [ {"id":row["id"],"capacity":row["capacity"],"allow_all":row["allow_all"],"allow_none":row["allow_none"]} for row in cursor.fetchall() ]
				return json.dumps(response)
			elif request.method == 'POST':
				data = request.get_json(force = True)
				o_container_class = entities.container_class(json_data = data)
				o_container_class.save(commit = True )
				return "Successfully created container_class"
		except:
			e = sys.exc_info()[1]
			return str(e)


	@app.route('/container_class/<id>',methods=['GET','PATCH','DELETE'])
	def container_class(id):

		try:
			if request.method == 'GET':
	
				o_container_class = entities.container_class(pk_tuple=(id,))
				return o_container_class.to_json()
			elif request.method == 'PATCH':
				data = request.get_json(force = True)
				o_container_class =entities.container_class(pk_tuple =(id,))
				o_container_class.change(data=data)
				o_container_class.save(commit=True)
				return "Successfully updated container_class, PK = {}".format("id:"+id)
			elif request.method == 'DELETE':
	
				o_container_class = entities.container_class(pk_tuple=(id,))
				o_container_class.delete(commit=True)
				return "Successfully deleted container_class, PK = {}".format("id:"+id)
		except:
			e = sys.exc_info()[1]
			return str(e)

	from . import db
	db.init_app(app)
	return app