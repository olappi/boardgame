"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template
from FlaskWebProject1 import app

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
    )

@app.route('/game/<gameId>')
def contact(gameId):
    """Renders the contact page."""
    return render_template(
        'game.html',
        title='Game!',
        year=datetime.now().year,
        message='This is a game.'
    )