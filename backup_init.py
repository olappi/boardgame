import os
import sys
from flask import Flask,request
from .db import get_db
import json


def create_app(test_config=None):
	# create and configure the app
	app = Flask(__name__, instance_relative_config=True)
	app.config.from_mapping(
		SECRET_KEY='dev',
		DATABASE=os.path.join(app.instance_path, 'boardgame.sqlite'),
	)

	if test_config is None:
		# load the instance config, if it exists, when not testing
		app.config.from_pyfile('config.py', silent=True)
	else:
		# load the test config if passed in
		app.config.from_mapping(test_config)

	# ensure the instance folder exists
	try:
		os.makedirs(app.instance_path)
	except OSError as e:
		print(e)


	@app.route('/')
	def schema():
		return '''{ 
		"player":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"game":{
			"fields":{
				"id":"integer",
				"name":"text",
				"created":"datetime",
				"board_id":"integer",
				"player1_id":"integer",
				"player2_id":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"board":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"play_area_template":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"play_area":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"cell_template":{
			"fields":{
				"id":"integer",
				"x":"integer",
				"y":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"cell":{
			"fields":{
				"id":"integer",
				"x":"integer",
				"y":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"terrain_type":{
			"fields":{
				"id":"integer",
				"name":"text",
				"speed":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"object_class":{
			"fields":{
				"id":"integer",
				"name":"text",
				"interactible":"boolean",
				"has_children":"boolean"
			},
			"key":{
				"id":"integer"
			}
		},
		"unit_class":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"factory_class":{
			"fields":{
				"id":"integer",
				"cool_down":"integer",
				"batch_size":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"resource_type":{
			"fields":{
				"id":"integer",
				"name":"text"
			},
			"key":{
				"id":"integer"
			}
		},
		"component_class":{
			"fields":{
				"id":"integer"
			},
			"key":{
				"id":"integer"
			}
		},
		"body_class":{
			"fields":{
				"id":"integer",
				"health":"integer",
				"size":"integer",
				"height":"integer",
				"width":"intger"
			},
			"key":{
				"id":"integer"
			}
		},
		"container_class":{
			"fields":{
				"id":"integer",
				"capacity":"integer",
				"allow_all":"boolean",
				"allow_none":"boolean"
			},
			"key":{
				"id":"integer"
			}
		} 
		}'''

	@app.route('/player/',methods=['GET','POST'])
	def player_collection():

		db = get_db()
		cursor = db.cursor()

		if request.method == 'GET':
			cursor.execute('SELECT * FROM player')
			response = [ {"id":row["id"],"name":row["name"]} for row in cursor.fetchall() ]
			return json.dumps(response)
		elif request.method == 'POST':
			data = request.get_json(force = True)
			data_tuple = tuple( [data["name"] if "name" in data else None] )
			

